import { Component } from 'react';
import { connect } from 'react-redux';

import template from './Headder.jsx';

class Headder extends Component {
   render() {
       return template.call(this);
   }
};

const mapStateToProps = state => {
    let { cart } = state.products;
    return { cart };
};

export default connect(mapStateToProps)(Headder);



