import React from 'react';
import {
   AppBar,
   Toolbar,
   Typography,
   TextField,
   Button,
   Badge
} from '@material-ui/core';
import ShoppingCart from '@material-ui/icons/ShoppingCart';

function Headder() {
   return (
       <div>
           <AppBar position="fixed" style={styles.headerStyle}>
               <Toolbar>
                   <Typography variant="title" color="inherit" style={styles.headerItems}>
                       e-commerce
               </Typography>
                   <div style={styles.searchContainer}>
                       <TextField placeholder="Mobiles, Electronics, Tablets" InputProps={{ disableUnderline: true }} type="text" style={styles.searchStyle}></TextField>

                   </div>

                   <div style={styles.headderButtons}>
                       <Button style={styles.headerItems}>Login</Button>
                       <Button style={styles.headerItems}>
                           <Badge badgeContent={this.props.cart} color="secondary">
                               <ShoppingCart />
                           </Badge>
                           Cart
                       </Button>
                   </div>
               </Toolbar>
           </AppBar>

       </div>
   )
}

const styles = {
   headerStyle: {
       backgroundColor: "#AD1457",
       color: "#424242",
       width: '100%'
   },
   headderButtons: {
       textAlign: 'right',
       flex: 1
   },
   searchStyle: {
       borderRadius: '10px',
       border: '2px solid #616161',
       paddingLeft: '1%',
       height: '4%',
       width: '90%',
       backgroundColor: 'white'
   },
   searchContainer: {
       flex: 3,
       textAlign: 'center'
   },
   headerItems: {
       color: 'white'
   }
}
export default Headder;

