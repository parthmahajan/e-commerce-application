import axios from 'axios';

export const getProducts = (maxPrice, brandFilters, limit) => {
    return (dispatch) => {
        const headers = {
            'Content-Type': 'application/json',
            "Access-Control-Allow-Origin": "*"
        };
        limit = limit ? limit : 6;
        let filterData = { maxPrice, brandFilters, limit };
        axios.post(`http://localhost:5000/customer/products/getProducts`, filterData, headers)
            .then(res => {
                dispatch({
                    type: 'GET_PRODUCTS',
                    payload: {data: res.data.data, maxPrice, brandFilters, limit}
                })
            });
    }
};

export const addToCart = () => {
    return {
        type: 'ADD_TO_CART'
    }
};