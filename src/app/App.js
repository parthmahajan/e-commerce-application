import React, { Component } from 'react';
import ProductCatelog from './pages/ProductCatelog';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducers from './reducers/index';
import thunk from 'redux-thunk';
import 'react-perfect-scrollbar/dist/css/styles.css';

const store = createStore(reducers, {}, applyMiddleware(thunk));

class App extends Component {
 render() {
   return (
     <Provider store={store}>
       <div className="App" style={{ flex: 1 }}>
         <ProductCatelog />
       </div>
     </Provider>
    
   );
 }
}

export default App;



