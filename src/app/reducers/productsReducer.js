const INITIAL_STATE = {
    productList: [],
    pagination: {
        limit: 6,
        from: 0,
    },
    filters: {
        priceSet: 4000,
        brandFilters: {}
    },
    cart: 0
   };

export default(state=INITIAL_STATE, action) => {
   switch(action.type) {
       case "GET_PRODUCTS":
            state.productList = action.payload.data;
            // Updating filters
            state.filters.priceSet = action.payload.maxPrice;
            state.filters.brandFilters = action.payload.brandFilters;
            // Updating pagination
            state.pagination.limit = action.payload.limit;
            console.log('limit', state.pagination);
           return { ...state };
        case "ADD_TO_CART":
           return { ...state, cart: state.cart + 1 };
        default: return state;
   }
};
