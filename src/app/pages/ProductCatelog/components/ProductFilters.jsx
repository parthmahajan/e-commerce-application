import React from 'react';
import PropTypes from 'prop-types';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import SendIcon from '@material-ui/icons/Send';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Slider from '@material-ui/lab/Slider';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';

const classes = {
 root: {
   width: '30%',
   marginTop: '1%',
   height: '100%',
   borderRight: '1px solid #BDBDBD'
 },
 sliderStyle: {
   width: '100%'
 },
 nested: {
   paddingLeft: '5px',
 },
};

function ProductFilters () {
   return (
     <div style={classes.root}>
       <List
         component="nav"
         subheader={<ListSubheader component="div">Nested List Items</ListSubheader>}
         style={{color: 'black'}}
       >
         <ListItem button onClick={this.handlePriceClick}>
           <ListItemIcon>
             <SendIcon />
           </ListItemIcon>
           <ListItemText inset primary="Price" />
           {this.state.openPrice ? <ExpandLess /> : <ExpandMore />}
         </ListItem>
         <Collapse in={this.state.openPrice} timeout="auto" unmountOnExit>
           <List component="div" disablePadding>
             <ListItem button style={classes.nested}>
               <div style={classes.sliderStyle}>
                 <Typography id="label">Max Price: Rs.{this.state.priceSet}</Typography>
                 <Slider value={this.state.sliderValue} aria-labelledby="label" onChange={this.onPriceChange} />
               </div>
              
             </ListItem>
           </List>
         </Collapse>

         <ListItem button onClick={this.handleClick}>
           <ListItemIcon>
             <InboxIcon />
           </ListItemIcon>
           <ListItemText inset primary="Brand" />
           {this.state.openBrands ? <ExpandLess /> : <ExpandMore />}
         </ListItem>
         <Collapse in={this.state.openBrands} timeout="auto" unmountOnExit>
           <List disablePadding>
             <ListItem>
               <Checkbox
                 checked={this.state.brandFilters['Apple']}
                 onChange={this.toggleBrandFilter('Apple')}
                 value="Apple"
               />
               <ListItemText inset primary="Apple" />
             </ListItem>
             <ListItem>
               <Checkbox
                 checked={this.state.brandFilters['Samsung']}
                 onChange={this.toggleBrandFilter('Samsung')}
                 value="Samsung"
               />
               <ListItemText inset primary="Samsung" />
             </ListItem>
           </List>
         </Collapse>

       </List>
     </div>
   );
}

ProductFilters.propTypes = {
 classes: PropTypes.object.isRequired,
};

export default ProductFilters;


