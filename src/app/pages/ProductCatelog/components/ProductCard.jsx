import React from "react";
import { Card,
   CardMedia,
   Typography,
   CardContent,
   Button,
   CardActions,
} from '@material-ui/core';

function ProductCard() {
   const imageUrl = require('../Images/iphoneX.jpg');
   let { price, title } = this.props;
 return (
   <div style={styles.containerStyle}>
     <Card style={styles.card}>
       <CardMedia
         style={styles.media}
         image={imageUrl}
         title={title}
       />
       <CardContent>
         <Typography gutterBottom variant="headline" component="h2">
           {price}
         </Typography>
         <Typography component="p">
           The latest product in market.
         </Typography>
       </CardContent>
       <CardActions>
         <Button size="small" color="primary">
           Buy Now
         </Button>
         <Button size="small" color="primary" onClick={this.addToCart}>
           Add to Cart
         </Button>
       </CardActions>
     </Card>
   </div>
 );
};

const styles = {
   containerStyle: {
       padding: '1%'
   },
   card: {
     minWidth: '100%',
     maxWidth: '100%',
   },
   media: {
     height: 10,
     paddingTop: '56.25%', // 16:9
   },
};
 export default ProductCard;



