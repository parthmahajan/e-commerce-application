import React from 'react';
import Headder from '../../../common/Headder';
import ProductList from '../containers/ProductList';
import ProductFilters from '../containers/ProductFilters';
import PerfectScrollbar from 'react-perfect-scrollbar';

function template () {
   return(
       <div style={styles.pageContainer}>
           <div style={{ display: 'flex' }}>
               <Headder />
           </div>
           <div style={styles.homeStyle}>
               <ProductFilters />
               <PerfectScrollbar  onYReachEnd = {() => this.getProducts()}>
                   <div style={styles.productList}>
                       <ProductList />
                   </div>
               </PerfectScrollbar>
           </div>
       </div>
   )
}

const styles = {
   homeStyle: {
       overflow: 'hidden',
       height: '100%',
       width: '100%',
       position: 'absolute',
       display: 'flex'
   },
   productList: {
       marginLeft: '15%',
       marginRight: '15%',
       marginTop: '6%'
   },
   pageContainer: {
       display: 'flex',
       flexDirection: 'column'
   }
}
export default template;


