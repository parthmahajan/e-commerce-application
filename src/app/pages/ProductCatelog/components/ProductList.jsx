import React from "react";
import ProductCard from '../containers/ProductCard';
import {
 GridList,
 GridListTile,
} from '@material-ui/core';

import 'react-perfect-scrollbar/dist/css/styles.css';

function template() {
 return (
     <div style={styles.root}>
     <GridList cellHeight={180} style={styles.gridList} cols={3}>
       {this.props.productList.map((product, index) => (<GridListTile key={index} style={styles.tile}>
         <ProductCard
           price = {product.price}
           title = {product.title}
         />
       </GridListTile>))}
     </GridList>
     </div>
 );
};

const styles ={
 root: {
   justifyContent: 'space-around',
   overflow: 'hidden',
   width: '100%',
   alignItems: 'center',
 },
 gridList: {
   width: '100%',
   height: '100%',
 },
 icon: {
   color: 'rgba(255, 255, 255, 0.54)',
 },
 tile: {
   height: '100%'
 }
};

export default template;



