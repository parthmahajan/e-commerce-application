import { Component } from 'react';
import { connect } from 'react-redux';

import template from '../components/ProductCatelog';
import { getProducts } from '../../../actions/products';

class ProductCatelog extends Component {
    getProducts = () => {
        let {getProducts, brandFilters, priceSet, limit} = this.props;
        limit += 6;
        getProducts(priceSet, brandFilters, limit);
    }
    render() {
       return template.call(this);
   }
};

const mapStateToProps = state => {
    let {brandFilters, priceSet} = state.products.filters;
    let { limit } = state.products.pagination;
    return {brandFilters, priceSet, limit};
}

const mapDispatchToProps = dispatch => ({
   getProducts: (maxPrice, brandFilters, limit) => dispatch(getProducts(maxPrice, brandFilters, limit))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductCatelog);



