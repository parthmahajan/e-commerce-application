import React    from "react";
import template from "../components/ProductList";
import { connect } from 'react-redux';
import {getProducts} from '../../../actions/products';

class ProductList extends React.Component {
 componentWillMount() {
   this.props.getProducts();
 }
 render() {
   return template.call(this);
 }
}
const mapStateToProps = state => {
 const { productList } = state.products;
 return { productList };
}

const mapDispatchToProps = dispatch => ({
 getProducts: () => dispatch(getProducts())
})
export default connect(mapStateToProps, mapDispatchToProps)(ProductList);



