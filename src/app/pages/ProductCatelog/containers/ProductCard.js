import React from "react";
import template from "../components/ProductCard";
import { connect } from 'react-redux';
import { addToCart } from '../../../actions/products';

class ProductCard extends React.Component {
    addToCart = () => {
        let { price, title, addToCart } = this.props;
        addToCart(price, title);
    };
    render() {
        return template.call(this);
    };
};


/* const mapDispatchToProps = dispatch => ({
    addToCart: () => dispatch(addToCart())
}) */

export default connect(null, { addToCart })(ProductCard);



