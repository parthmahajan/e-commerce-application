import React from "react";
import template from "../components/ProductFilters";
import { connect } from 'react-redux';
import { getProducts } from '../../../actions/products';
import _ from 'lodash';

class ProductFilters extends React.Component {
   constructor() {
       super();
       this.state = {
           openBrands: true,
           openPrice: true,
           priceSet: 40000,
           sliderValue: 40,
           brandFilters: {},
           activeFilters: []
       };
   }
   handleClick = () => {
       this.setState(state => ({ openBrands: !state.openBrands }));
   };
   handlePriceClick = () => {
       this.setState(state => ({ openPrice: !state.openPrice }));
   };
   onPriceChange = (event, value) => {
       let { activeFilters } = { ...this.state };
       this.setState(state => ({ sliderValue: value, priceSet: value * 1000 }));
       activeFilters.length !== 0 ? this.props.getProducts(this.state.priceSet, this.state.brandFilters) : this.props.getProducts(this.state.priceSet, {});
   };
   toggleBrandFilter = name => async event => {
       let { brandFilters, activeFilters } = { ...this.state };

       brandFilters[name] = event.target.checked;
       (activeFilters.indexOf(name) === -1) ? activeFilters.push(name) : delete activeFilters[activeFilters.indexOf(name)];
       activeFilters = _.compact(activeFilters);

       await this.setState({ brandFilters, activeFilters });
       activeFilters.length !== 0 ? this.props.getProducts(this.state.priceSet, this.state.brandFilters) : this.props.getProducts(this.state.priceSet, {});
   };
   render() {
       return template.call(this);
   }
}
const mapDispatchToProps = dispatch => ({
   getProducts: (maxPrice, brandFilters) => dispatch(getProducts(maxPrice, brandFilters)),
})
export default connect(null, mapDispatchToProps)(ProductFilters);



