import React, { Component } from 'react';
import Headder from '../../common/Headder';
import ProductList from '../ProductList';

class Home extends Component {
   render() {
       return(
           <div style={{ flex: 1 }}>
               <Headder/>
               <div style={styles.homeStyle}>
                   <ProductList/>
               </div>
           </div>
          
       )
   }
}
const imageUrl = require('../../../images/homeBg.jpg');
const styles = {
   homeStyle: {
       backgroundImage: 'url(' + imageUrl + ')',
       backgroundSize: 'cover',
       overflow: 'hidden',
       height: '100%',
       width: '100%',
       position: 'absolute'
   }
}
export default Home;


